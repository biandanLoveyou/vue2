// 该文件专门用于创建整个应用的路由器
import VueRouter from 'vue-router'
//引入组件
import Travel from '../pages/Travel'
import Food from '../pages/Food'
import Shenzhen from '../pages/Shenzhen'
import Beijing from '../pages/Beijing'
import Detail from '../pages/Detail'

//创建一个路由器
const router = new VueRouter({
    routes: [
        {
            path: '/travel',
            component: Travel,
            //路由元信息，可以添加自定义数据
            meta: { isAuth: false, title: "旅行" },
            // 通过 children 配置子路由
            children: [
                {
                    // 此处一定不要加 / ，不能写成 /shenzhen
                    path: "shenzhen",
                    component: Shenzhen,
                    //路由元信息，可以添加自定义数据
                    meta: { isAuth: true, title: "旅行·深圳" },
                    children: [
                        {
                            // 给路由命名
                            name: "shenzhenDetail",
                            //使用 params 的方式，需要提前声明接收参数，写好占位符
                            path: "detail/:id/:name",
                            component: Detail,
                            //函数写法的第一种写法：使用 $route 接收参数（如果使用 params 接收参数，就用 $route.params。如果使用 query 接收参数，就用 $route.query）
                            props($route) {
                                return {
                                    id: $route.params.id,
                                    name: $route.params.name,
                                }
                            }

                        }
                    ]
                },
                {
                    // 此处一定不要加 / ，不能写成 /beijing
                    path: "beijing",
                    component: Beijing,
                    meta: { isAuth: true, title: "旅行·北京" },
                    children: [
                        {
                            // 给路由命名
                            name: "beijingDetail",
                            //使用 params 的方式，需要提前声明接收参数，写好占位符
                            path: "detail/:id/:name",
                            component: Detail,
                            //props的第三种写法，值为函数
                            //函数写法的第一种写法：使用 $route 接收参数（如果使用 params 接收参数，就用 $route.params。如果使用 query 接收参数，就用 $route.query）
                            props($route) {
                                return {
                                    id: $route.params.id,
                                    name: $route.params.name,
                                }
                            }

                        }
                    ]
                }
            ]
        },
        {
            path: '/food',
            component: Food,
            //路由元信息，可以添加自定义数据
            meta: { isAuth: false, title: "美食" },
        }
    ]
})

//全局前置路由守卫：初始化的时候被调用，每次路由切换之前被调用
//有3个参数：to：要切换的目标路由；from：来自哪个路由组件；next：放行
router.beforeEach((to, from, next) => {
    console.log("前置路由守卫", to, from);
    //判断是否需要鉴权（从 meta 的元信息中判断）
    if (to.meta.isAuth) {
        //实际开发中从服务端获取数据进行判断（这里我们从浏览器缓存里模拟数据）
        if (localStorage.getItem("userName") === "流放深圳") {
            next();
        } else {
            alert("Sorry，用户名不正确，你无权查看此页面！");
        }
    } else {
        next();
    }
})

//全局后置路由守卫：初始化的时候被调用，每次路由切换之后被调用
router.afterEach((to, from) => {
    console.log("后置路由守卫", to, from);
    //设置网页标题
    document.title = to.meta.title || "旅行网";
})

//暴露一个路由器
export default router;