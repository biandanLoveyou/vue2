// 引入 Vue 文件
import Vue from 'vue'
//引入 App 组件，它是所有组件的父组件
import App from './App.vue'

//完整引入
//引入 ElementUI 组件库
//import ElementUI from "element-ui";
//引入ElementUI全部样式
//import "element-ui/lib/theme-chalk/index.css";

//按需引入 ElementUI 组件库
import { Button, Row, DatePicker } from "element-ui";
Vue.component(Button.name, Button);
Vue.component(Row.name, Row);
Vue.component(DatePicker.name, DatePicker);

/**
 * 或者使用下面的写法
Vue.use(Button);
Vue.use(Row);
Vue.use(DatePicker);
 */

// 关闭 Vue 的生产提示
Vue.config.productionTip = false

// 创建 Vue 实例对象
new Vue({
  // 将 app 组件放入 vm 容器中
  render: h => h(App),
}).$mount('#root') 
