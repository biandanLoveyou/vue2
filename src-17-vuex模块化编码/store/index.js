//引入 Vue 核心库
import Vue from "vue";
//引入 Vuex
import Vuex from "vuex";
//引入自定义的计算js库
import calculateOptions from "./calculate";
import movieOptions from "./movie";
//应用 Vuex 插件
Vue.use(Vuex);

//创建并暴露 store
export default new Vuex.Store({
    modules: {
        calculateModule: calculateOptions,
        movieModule: movieOptions,
    }
})