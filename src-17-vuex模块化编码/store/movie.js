//电影相关逻辑
import axios from "axios";
export default {
    //使用命名空间
    namespaced: true,
    actions: {
        //带【狼】字电影名的判断
        addMovieLang(context, value) {
            if (value.name.indexOf("狼") >= 0) {
                context.commit("ADD_MOVIE", value);
            } else {
                alert("添加的电影名称必须带【狼】字");
            }
        },
        //从服务器中获取数据
        addMovieServer(context) {
            axios.get("http://localhost:8000/helloWorld")
                .then(
                    response => {
                        let randomId = Math.random().toString(36).substring(2, 9);
                        context.commit("ADD_MOVIE", { id: randomId, name: response.data });
                    },
                    error => {
                        alert(error.message);
                    }
                )
        },
    },
    mutations: {
        //添加电影名称
        ADD_MOVIE(state, value) {
            console.log("【添加电影名称】mutations中的 ADD_MOVIE 被调用了");
            state.movieList.unshift(value);
        }
    },
    state: {
        movieList: [
            { id: "001", name: "《功夫》" }
        ]
    },
    getters: {
        firstMovieName(state) {
            return state.movieList[0].name;
        }
    }
}