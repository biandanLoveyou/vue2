// 引入 Vue 文件
import Vue from 'vue'

//引入 App 组件，它是所有组件的父组件
import App from './App.vue'

//引入 store
import store from "./store";

// 关闭 Vue 的生产提示
Vue.config.productionTip = false

// 创建 Vue 实例对象
new Vue({
  // 将 app 组件放入 vm 容器中
  render: h => h(App),
  store,
}).$mount('#root') 
