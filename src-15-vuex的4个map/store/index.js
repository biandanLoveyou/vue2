//引入 Vue 核心库
import Vue from "vue";
//引入 Vuex
import Vuex from "vuex";
//应用 Vuex 插件
Vue.use(Vuex);

//准备 Actions 对象：响应组件中用户的动作
const actions = {
    //两数相加
    increment(context, value) {
        console.log("【两数相加】actions中的 increment 被调用了");
        context.commit("INCREMENT", value);
    },
    //两数相减
    decrement(context, value) {
        console.log("【两数相减】actions中的 decrement 被调用了");
        context.commit("DECREMENT", value);
    },
};

//准备 Mutations 对象：修改 State 中的数据
const mutations = {
    //两数相加
    INCREMENT(state, value) {
        console.log("【两数相加】mutations中的 INCREMENT 被调用了");
        state.sum += value;
    },
    //两数相减
    DECREMENT(state, value) {
        console.log("【两数相减】mutations中的 DECREMENT 被调用了");
        state.sum -= value;
    },

    //相加的结果为奇数时再加
    INCREMENT_ODD(state, value) {
        console.log("【相加的结果为奇数时再加】mutations中的 INCREMENT_ODD 被调用了");
        if (state.sum % 2 === 1) {
            state.sum += value;
        }
    },
    //延时相加
    INCREMENT_WAIT(state, value) {
        console.log("【延时相加】mutations中的 INCREMENT_Wait 被调用了");
        setTimeout(() => {
            state.sum += value;
        }, 500)
    },
};

//准备 State 对象：保存具体的数据
const state = {
    //当前的和
    sum: 0,
    pai: 3.141592654,
    gouGu: "勾三股四弦五",
    triangle: "三角形面积计算公式：底*高/2"
};

// 准备 getters：用于将 state 中的数据进行加工
const getters = {
    //将数据放大 10 倍
    enlarge(state) {
        return state.sum * 10;
    },
    //三角形面积公式
    area(state) {
        return state.triangle;
    }
}

//创建并暴露 store
export default new Vuex.Store({
    actions,
    mutations,
    state,
    getters,
})