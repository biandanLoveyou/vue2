const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  //关闭在开发环境下每次保存代码时都进行代码质量检查
  lintOnSave: false,
  devServer: {
    // 简单代理配置：对应服务端的协议、地址和端口号
    // proxy: "http://localhost:8000"
    // 多代理配置
    proxy: {
      // 匹配所有以 api 开头的请求
      "/api": {
        target: "http://localhost:8000",//代理目标的基础路径
        changeOrigin: true,//默认值true。设置为true的话，后端服务器收到的请求头中的host为8000；设置为false，接收到host为8080。
        pathRewrite: {//替换请求路径，key值是正则表达式
          "^/api": ""
        },
        ws: true,//开启websocket协议，默认开启 true
      },
      "/study": {
        target: "http://localhost:8000",//代理目标的基础路径
        pathRewrite: {//替换请求路径，key值是正则表达式
          "^/study": ""
        }
      }
    }
  }
})
