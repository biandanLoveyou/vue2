// 引入 Vue 文件
import Vue from 'vue'
//引入 App 组件，它是所有组件的父组件
import App from './App.vue'
//引入VueRouter
import VueRouter from 'vue-router'
//引入路由器
import router from './router'
// 关闭 Vue 的生产提示
Vue.config.productionTip = false

//应用插件
Vue.use(VueRouter)

// 创建 Vue 实例对象
new Vue({
  // 将 app 组件放入 vm 容器中
  render: h => h(App),
  router: router
}).$mount('#root') 
