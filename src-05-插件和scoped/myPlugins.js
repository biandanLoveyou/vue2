export default {
    //install方法可以包含多个自定义参数
    install(Vue, name, age){
        console.log(name, age);

        //可以实现：全局过滤器
        Vue.filter("mySlice", function(value){
            return value.slice(0, 4);
        });

        //可以实现：定义全局指令
        Vue.directive("fbind", {
            //指令与元素成功绑定时（一上来）
            bind(element, binding){
                element.value = binding.value;
            },
            //指令所在元素被插入页面时
            inserted(element, binding){
                element.focus();
            },
            //指令所在的模板被重新解析时
			update(element,binding){
				element.value = binding.value;
                element.focus();
			}
        });

        //可以实现：混入
        Vue.mixin({
            data(){
                return {
                    honor: "2016年，获得第89届奥斯卡金像奖终身成就奖",
                    birthday: "2024年70岁",
                }
            }
        });

        //可以实现：给 Vue 原型上添加一个方法，vm 和 vc 都可以使用
        Vue.prototype.say = () =>{console.log("大家好啊！")}
    }
}