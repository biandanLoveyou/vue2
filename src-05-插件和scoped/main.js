// 引入 Vue 文件
import Vue from 'vue'

//引入 App 组件，它是所有组件的父组件
import App from './App.vue'
import myPlugins from './myPlugins'

// 关闭 Vue 的生产提示
Vue.config.productionTip = false

//使用插件
Vue.use(myPlugins, "成龙", "70岁")

// 创建 Vue 实例对象
new Vue({
  // 将 app 组件放入 vm 容器中
  render: h => h(App),
}).$mount('#root') 
