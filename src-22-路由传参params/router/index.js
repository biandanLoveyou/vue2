// 该文件专门用于创建整个应用的路由器
import VueRouter from 'vue-router'
//引入组件
import Travel from '../pages/Travel'
import Food from '../pages/Food'
import Shenzhen from '../pages/Shenzhen'
import Beijing from '../pages/Beijing'
import Detail from '../pages/Detail'

//创建并暴露一个路由器
export default new VueRouter({
    routes: [
        {
            path: '/travel',
            component: Travel,
            // 通过 children 配置子路由
            children: [
                {
                    // 此处一定不要加 / ，不能写成 /shenzhen
                    path: "shenzhen",
                    component: Shenzhen,
                    children: [
                        {
                            // 给路由命名
                            name: "shenzhenDetail",
                            //使用 params 的方式，需要提前声明接收参数，写好占位符
                            path: "detail/:id/:name",
                            component: Detail
                        }
                    ]
                },
                {
                    // 此处一定不要加 / ，不能写成 /beijing
                    path: "beijing",
                    component: Beijing,
                    children: [
                        {
                            // 给路由命名
                            name: "beijingDetail",
                            //使用 params 的方式，需要提前声明接收参数，写好占位符
                            path: "detail/:id/:name",
                            component: Detail
                        }
                    ]
                }
            ]
        },
        {
            path: '/food',
            component: Food
        }
    ]
})
