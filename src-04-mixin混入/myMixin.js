// 定义混入：演员信息
export const actor = {
    mounted(){
        console.log("大家好，我叫成龙。", this.$el);
    },
    methods:{
        showAuth(){
            console.log("这是我的代表作：", this.famous);
        }
    }
}

// 定义混入：其它信息
export const info = {
    data(){
        return{
            honor: "国家一级演员",
            birthday: "1954年"
        }
    }
}