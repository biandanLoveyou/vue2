//引入 Vue 核心库
import Vue from "vue";
//引入 Vuex
import Vuex from "vuex";
//应用 Vuex 插件
Vue.use(Vuex);

//准备 Actions 对象：响应组件中用户的动作
const actions = {
    //两数相加
    increment(context, value) {
        console.log("【两数相加】actions中的 increment 被调用了");
        context.commit("INCREMENT", value);
    },

};

//准备 Mutations 对象：修改 State 中的数据
const mutations = {
    //两数相加
    INCREMENT(state, value) {
        console.log("【两数相加】mutations中的 INCREMENT 被调用了");
        state.sum += value;
    },
    //添加电影名称
    ADD_MOVIE(state, value) {
        console.log("【添加电影名称】mutations中的 ADD_MOVIE 被调用了");
        state.movieList.unshift(value);
    }
};

//准备 State 对象：保存具体的数据
const state = {
    //当前的和
    sum: 0,
    movieList: [
        { id: "001", name: "《功夫》" }
    ]
};

// 准备 getters：用于将 state 中的数据进行加工
const getters = {
    //将数据放大 10 倍
    enlarge(state) {
        return state.sum * 10;
    },
}

//创建并暴露 store
export default new Vuex.Store({
    actions,
    mutations,
    state,
    getters,
})