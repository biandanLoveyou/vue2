// 该文件专门用于创建整个应用的路由器
import VueRouter from 'vue-router'
//引入组件
import Travel from '../pages/Travel'
import Food from '../pages/Food'

//创建并暴露一个路由器
export default new VueRouter({
    routes: [
        {
            path: '/travel',
            component: Travel
        },
        {
            path: '/food',
            component: Food
        }
    ]
})
