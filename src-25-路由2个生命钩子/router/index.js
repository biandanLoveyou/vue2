// 该文件专门用于创建整个应用的路由器
import VueRouter from 'vue-router'
//引入组件
import Travel from '../pages/Travel'
import Food from '../pages/Food'
import Shenzhen from '../pages/Shenzhen'
import Beijing from '../pages/Beijing'
import Detail from '../pages/Detail'

//创建并暴露一个路由器
export default new VueRouter({
    routes: [
        {
            path: '/travel',
            component: Travel,
            // 通过 children 配置子路由
            children: [
                {
                    // 此处一定不要加 / ，不能写成 /shenzhen
                    path: "shenzhen",
                    component: Shenzhen,
                    children: [
                        {
                            // 给路由命名
                            name: "shenzhenDetail",
                            //使用 params 的方式，需要提前声明接收参数，写好占位符
                            path: "detail/:id/:name",
                            component: Detail,
                            //props 的第一种写法（数据固定，极少使用），值为对象，该对象中的所有key-value都会以props的形式传给Detail组件。
                            //props: { id: "siheyuan", name: "四合院" }

                            //props 的第二种写法，值为布尔值，若布尔值为真，就会把该路由组件收到的所有 params 参数，以props的形式传给Detail组件。
                            //props: true

                            //props的第三种写法，值为函数
                            //函数写法的第一种写法：使用 $route 接收参数（如果使用 params 接收参数，就用 $route.params。如果使用 query 接收参数，就用 $route.query）
                            // props($route) {
                            //     return {
                            //         id: $route.params.id,
                            //         name: $route.params.name,
                            //     }
                            // }

                            //函数写法的第二种写法：拿到参数的时候，就结构赋值
                            // props({ params }) {
                            //     return {
                            //         id: params.id,
                            //         name: params.name,
                            //     }
                            // }

                            //函数写法的第三种写法：使用结构赋值的连续写法
                            props({ params: { id, name } }) {
                                return { id, name }
                            }
                        }
                    ]
                },
                {
                    // 此处一定不要加 / ，不能写成 /beijing
                    path: "beijing",
                    component: Beijing,
                    children: [
                        {
                            // 给路由命名
                            name: "beijingDetail",
                            //使用 params 的方式，需要提前声明接收参数，写好占位符
                            path: "detail/:id/:name",
                            component: Detail,
                            //props 的第一种写法（数据固定，极少使用），值为对象，该对象中的所有key-value都会以props的形式传给Detail组件。
                            //props: { id: "siheyuan", name: "四合院" }

                            //props 的第二种写法，值为布尔值，若布尔值为真，就会把该路由组件收到的所有 params 参数，以props的形式传给Detail组件。
                            //props: true

                            //props的第三种写法，值为函数
                            //函数写法的第一种写法：使用 $route 接收参数（如果使用 params 接收参数，就用 $route.params。如果使用 query 接收参数，就用 $route.query）
                            // props($route) {
                            //     return {
                            //         id: $route.params.id,
                            //         name: $route.params.name,
                            //     }
                            // }

                            //函数写法的第二种写法：拿到参数的时候，就结构赋值
                            // props({ params }) {
                            //     return {
                            //         id: params.id,
                            //         name: params.name,
                            //     }
                            // }

                            //函数写法的第三种写法：使用结构赋值的连续写法
                            props({ params: { id, name } }) {
                                return { id, name }
                            }
                        }
                    ]
                }
            ]
        },
        {
            path: '/food',
            component: Food
        }
    ]
})
